const fs = require('fs')

/*
    Problem 2:    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt  
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt  
        3. Read the new file and convert it to lower case. Then split the contents into sentences. 
            Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const readFile = (path) => {
   
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf-8', (error, data) => {
            if (error) {
                // console.log(path)
                reject(error)
            } else {
                console.log(`${path} file was read sucessfully! `)
                resolve(data)
            }
        })
    })
}
const sortData = (data) => {
    return new Promise((resolve, reject) => {
        if (data) {
            data = data.split('\n').sort((a, b) => {
                if (a < b) {
                    return -1;
                } else if (a > b) {
                    return 1;
                } else {
                    return 0;
                }
            })
            console.log(`Data sorted sucessfully! `)
            resolve(data.join('\n'))
        }else{
            reject("data not found 2!")
        }

    })
}
const convertUpperCase = (data) => {
    return new Promise((resolve, reject) => {
        if (data) {
            console.log(`data converted to upperCase sucessfully! `)
            resolve(data.toUpperCase())

        } else {
            reject("There is no data present")
        }
    })

}

const convertLowerCase = (data) => {
    // console.log(data)
    return new Promise((resolve, reject) => {
        if (data) {
            console.log(`data convered to lowerCase sucessfully! `)
            resolve(data.toLowerCase())

        } else {
            reject("There is no data present")
        }
    })

}
const writeFile = (path, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, error => {
            if (error) {
                reject(error)
            } else {
                console.log(`${path} file was written sucessfully! `)
                resolve()
            }
        })
    })

}
const appendFilename = (fileName) => {
    return new Promise((resolve, reject) => {
        fs.appendFile('filenames.txt', fileName+"\n", (error) => {
            if (error) {
                reject(error)
            } else {
                console.log(`${fileName} appended sucessfully! `)
                resolve()
            }

        });

    })

}
const splitData = (data) => {

    return new Promise((resolve, reject) => {
        if (data) {
            data = data.split('.')
            data = data.reduce((accum, currentValue) => {
                currentValue = currentValue.trim();
                accum += currentValue;
                accum += '\n'
                return accum

            }, "")
            console.log("spliting data sucessfully !")
            resolve(data)
        } else {
            reject("Data Not found!")
        }

    })

}
deleteFile = (path) => {    
    return new Promise((resolve,reject) => {
        fs.unlink(path,error => {
            if(error){
                reject(error)
            }else{
                console.log(`${path} file was deleted sucessfully! `)
                resolve()
            }
        })
    })
}
const deleteAllFile = (data) => {
    
    return new Promise((resolve,reject) => {
        if(data){
            const arr = []
            let string = ""
            for(let i = 0 ;i<data.length ; i++){
                if(data[i] != '\n'){
                    string+=data[i]
                }else{
                   arr.push(string)
                    string = ""
                }

            }    
            arr.forEach(fileName => deleteFile(fileName).catch(error => reject(error)))
            resolve()
        }else{
            reject("Data not present!")
        }
    })
}
const problem2 = (path) => {
    return new Promise((resolve,reject) => {
        readFile(path)
        .then(data => convertUpperCase(data))
        .then((data) => writeFile('./upperCase.txt', data))
        .then(() => appendFilename('upperCase.txt'))
        .then(() => readFile('./upperCase.txt'))
        .then((data) => convertLowerCase(data))
        .then((data) => splitData(data))
        .then((data) => writeFile('./lowerCase.txt', data))
        .then(() => appendFilename('lowerCase.txt'))
        .then(() => readFile('./lowerCase.txt'))
        .then((data) => sortData(data))
        .then((data) =>  writeFile('./sortedData.txt', data) )
        .then(() => appendFilename('sortedData.txt'))
        .then(() => readFile('./filenames.txt'))
        .then(data => deleteAllFile(data))
        .then(() => resolve())       
        .catch(error =>reject(error))
        .finally(() => fs.unlink('../test/filenames.txt', error => {
            if(error){
                console.log("No such file exist")
            }else {
                console.log("Finally every thing is fine")
            }
            
        }))

    })
    

}

module.exports = problem2