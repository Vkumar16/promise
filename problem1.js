const fs = require('fs')
/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
makeDir = () => {
    return new Promise((resolve, reject) => {
        fs.mkdir('random', error => {
            if (error) {
                reject(error)
                return
            } else {
                console.log("directory created!")
                resolve()
            }
        })
    })
}
writeFile = (path) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, "abcd", error => {
            if (error) {
                reject(error)
            } else {
                console.log("file created!")
                resolve()
            }
        })
    })
}
deleteFile = (path) => {
    return new Promise((resolve, reject) => {
        fs.unlink(path, error => {
            if (error) {
                reject(error)
            } else {
                console.log("file deleted!")
                resolve()
            }
        })
    })
}
problem1 = (num) => {
    return new Promise((resolve,reject) => {
        makeDir()
        .then(() => {
            let arr = []
            let arr1 = []
            for (let i = 0; i < num; i++) {
                arr.push(writeFile(`./random/randomFile${i}`))
                arr1.push(deleteFile(`./random/randomFile${i}`))
            }
            Promise.all(arr)
                .then(() => console.log("all File written sucessfully! "))
                .catch((error) => console.log(error))

            Promise.all(arr1)
                .then(() => console.log("all File deleted sucessfully! "))
                .catch((error) => console.log(error))
        })
        .catch(error => reject(error))
        .finally(() => console.log("finally block is running"))


    })
    }
module.exports = problem1